/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tatar.sd_week_10;
import java.util.*;
import java.sql.*;
/**
 *
 * @author tatar
 */
public class DeleteUser {
    public static void main(String [] args){
        Scanner kb = new Scanner(System.in);
        Connection conn = null;        
        Statement stmt = null;
        String dbName = "user.db";
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:"+dbName);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            System.out.println("you ID to delete :");
            int selID=kb.nextInt();
            System.out.println("-----------------------");
            //Delete part
            stmt.executeUpdate("DELETE FROM USER WHERE ID = "+selID+"");
            conn.commit();
            //Select part
            ResultSet rs = stmt.executeQuery("SELECT * FROM USER");
            
            while(rs.next()){
                int id = rs.getInt("id");
                System.out.println("ID : "+id);
                String username = rs.getString("username");
                System.out.println("Username : "+username);
                String password = rs.getString("password");
                System.out.println("Password : "+password);                    
                System.out.println("-----------------------");
            }
            
            rs.close();
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException ex) {
            System.out.println("Library org.sqlite.JDBC not found!?");
            System.exit(0);
        } catch (SQLException ex) {
            System.out.println("Can't open database! :(");
            System.exit(0);
        }
        System.out.println("Delete complete :)");
    }
}
