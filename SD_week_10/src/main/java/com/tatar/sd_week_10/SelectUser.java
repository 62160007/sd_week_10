/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tatar.sd_week_10;
import java.sql.*;
/**
 *
 * @author tatar
 */
public class SelectUser {
    public static void main(String [] args){
        Connection conn = null;        
        Statement stmt = null;
        String dbName = "user.db";
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:"+dbName);
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM USER");
            
            while(rs.next()){
                int id = rs.getInt("id");
                System.out.println("ID : "+id);
                String username = rs.getString("username");
                System.out.println("Username : "+username);
                String password = rs.getString("password");
                System.out.println("Password : "+password);                    
                System.out.println("-----------------------");
            }
            
            rs.close();
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException ex) {
            System.out.println("Library org.sqlite.JDBC not found!?");
            System.exit(0);
        } catch (SQLException ex) {
            System.out.println("Can't open database! :(");
            System.exit(0);
        }
        System.out.println("Select complete :)");
    }
}
