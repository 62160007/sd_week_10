/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tatar.sd_week_10;
import java.util.*;
import java.sql.*;
/**
 *
 * @author tatar
 */
public class CreateTableCompany {
    public static void main(String [] args){
        Connection conn = null;        
        Statement stmt = null;
        String dbName = "user.db";
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:"+dbName);
            stmt = conn.createStatement();
            String sql = "CREATE TABLE COMPANY"+
                    "(ID INTEGER PRIMARY KEY AUTOINCREMENT,"+
                    "NAME TEXT NOT NULL,"+
                    "AGE INT NOT NULL,"+
                    "ADDRESS CHAR(50),"+
                    "SALARY REAL)";
            stmt.executeUpdate(sql);
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException ex) {
            System.out.println("Library org.sqlite.JDBC not found!?");
            System.exit(0);
        } catch (SQLException ex) {
            System.out.println("Can't open database! :(");
            System.exit(0);
        }
        System.out.println("Successfully to Connect database "+dbName+" :)");
        System.out.println("Create Table complete :)");
    }
}
