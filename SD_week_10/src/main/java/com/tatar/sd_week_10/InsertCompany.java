/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tatar.sd_week_10;
import java.util.*;
import java.sql.*;
/**
 *
 * @author tatar
 */
public class InsertCompany {
    public static void main(String [] args){
        Scanner kb = new Scanner(System.in);
        Connection conn = null;        
        Statement stmt = null;
        String dbName = "user.db";
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:"+dbName);
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
           
            /*String sql = "INSERT INTO COMPANY(NAME,AGE,ADDRESS,SALARY)"+
"VALUES('Paul',32,'California',20000.00);";*/
            System.out.println("Insert \nName :");
            String name = kb.next();
            System.out.println("Age :");
            int age = kb.nextInt();
            System.out.println("Address :");                    
            String address = kb.next();
            System.out.println("Salary :");
            double salary = kb.nextDouble();
            String sql = "INSERT INTO COMPANY(NAME,AGE,ADDRESS,SALARY)"+
"VALUES('"+name+"',"+age+",'"+address+"',"+salary+");";
            stmt.executeUpdate(sql);
            conn.commit();
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException ex) {
            System.out.println("Library org.sqlite.JDBC not found!?");
            System.exit(0);
        } catch (SQLException ex) {
            System.out.println("Can't open database! :(");
            System.exit(0);
        }
        System.out.println("Insert Complete! :)");
    }
}
