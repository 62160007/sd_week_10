/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tatar.sd_week_10;
import java.util.*;
import java.sql.*;
/**
 *
 * @author tatar
 */
public class DeleteCompany {
    public static void main(String [] args){
        Scanner kb = new Scanner(System.in);
        Connection conn = null;        
        Statement stmt = null;
        String dbName = "user.db";
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:"+dbName);
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            System.out.println("you ID to delete :");
            int selID=kb.nextInt();
            System.out.println("-----------------------");
            //Delete part
            stmt.executeUpdate("DELETE FROM COMPANY WHERE ID = "+selID+"");
            conn.commit();
            //Select part
            ResultSet rs = stmt.executeQuery("SELECT * FROM COMPANY");
            
            while(rs.next()){
                int id = rs.getInt("id");
                System.out.println("ID : "+id);
                String name = rs.getString("name");
                System.out.println("Name : "+name);
                int age = rs.getInt("age");
                System.out.println("Age : "+age);
                String address = rs.getString("address");
                System.out.println("Address : "+address);                    
                double salary = rs.getDouble("salary");
                System.out.println("Salary : "+salary);
                System.out.println("-----------------------");
            }
            
            rs.close();
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException ex) {
            System.out.println("Library org.sqlite.JDBC not found!?");
            System.exit(0);
        } catch (SQLException ex) {
            System.out.println("Can't open database! :(");
            System.exit(0);
        }
        System.out.println("Delete complete :)");
    }
}
